#!/usr/bin/env bash

set -euo pipefail

WEB_DIR=/opt/developer.empaia.org

mkdir -p ${WEB_DIR}
rm -r ${WEB_DIR}
mkdir ${WEB_DIR}
cp -R public/* ${WEB_DIR}

<!-- _coverpage.md -->

![logo](../images/logo.png)

> EMPAIA App Developer Documentation

<a href="v3">Version 3</a>
<br><br><b>Release History</b>:
<br>
<a class="button_greyed" href="v2">Version 2</a>
<br>
<a class="button_greyed" href="draft-3">Version 1: Draft-3</a>
<br>
<a class="button_greyed" href="draft-2">Version 1: Draft-2</a>
<br>
<a class="button_greyed" href="draft-1">Version 1: Draft-1</a>

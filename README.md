# App Developer Documentation

This repository includes the documentation for app developers.

The raw markdown documentation is located at `public/app_developer_docs/draft-x/README.md`

Viewing using a webserver ist highly recommended:

```bash
cd public
python3 -m http.server 8000
```

Go to [http://localhost:8000](http://localhost:8000)

## Clone / Pull with submodules

```bash
git clone --recurse-submodules https://gitlab.cc-asp.fraunhofer.de/empaia/app-test-suite/developer-portal.git
```

```bash
git pull --recurse-submodules
```

## Adding new versions of docs

To add a new version of any docs, use the submodules feature. E.g. to add a `draft-x` version of App-Developer-Documentation (https://gitlab.cc-asp.fraunhofer.de/empaia/app-test-suite/app-developer-documentation/):

```bash
git submodule add https://gitlab.cc-asp.fraunhofer.de/empaia/app-test-suite/app-developer-documentation/ ./public/app_developer_docs/draft-x/
```

## OpenAPI.json Files

Bug in redoc when using union and explicit request example:

https://github.com/Redocly/redoc/issues/1429

Workaround:

run `fix_openapi_for_redoc.py -i some_openapi.json -o fixed_some_openapi.json` to remove explicit example, e.g. when updating `public/docs/draft-1/specs/app_service_redoc/openapi.json`.

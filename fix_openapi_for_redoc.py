# functionality in fastapi to provide example post data gets mixed up in redoc (generates own example data) when response is Union.
# script removes "example" property where union ist used (translates in openapi.json to "anyOf"), e.g.

import json
import argparse
from pathlib import Path
from pprint import pprint


def is_valid_file(parser, filename):
    path = Path(filename).expanduser()
    if not path.is_file():
        parser.error("The file %s does not exist!" % filename)
    else:
        return Path(filename)


def get_path(parser, filename):
    path = Path(filename).expanduser()
    return path


def traverse(indict, pre=None):
    pre = pre[:] if pre else []
    if isinstance(indict, dict):
        for key, value in indict.items():
            if isinstance(value, dict):
                for d in traverse(value, pre + [key]):
                    yield d
            elif isinstance(value, list) or isinstance(value, tuple):
                for v in value:
                    for d in traverse(v, pre + [key]):
                        yield d
            else:
                pass
                yield pre + [key]
    else:
        yield pre + [indict]


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Fixes OpenAPI openapi.json specs to be displayed in redoc.")
    parser.add_argument("-i", dest="in_file", required=True,
                        help="input file", metavar="FILE",
                        type=lambda x: is_valid_file(parser, x))
    parser.add_argument("-o", dest="out_file", required=True,
                        help="output file", metavar="FILE",
                        type=lambda x: get_path(parser, x))
    args = parser.parse_args()

    with args.in_file.open() as f:
        myjson = json.load(f)

    paths = []
    for path in traverse(myjson):
        if "anyOf" in path or "allOf" in path:
            paths.append(path)

    # for path in paths:
    #     trace = myjson
    #     for key in path:
    #         print(key)
    #         print(trace)
    #         trace = trace[str(key)]
    #         print(trace)


    for path in paths:
        pos = myjson
        for key in path:
            if str(key) == "anyOf" or str(key) == "allOf":
                # print(pos[str(key)])
                try:
                    del pos["example"]
                except (KeyError, TypeError):
                    pass
                break
            try:
                pos = pos[str(key)]
            except Exception:
                # pprint(pos)
                pass
    
    with args.out_file.open("w") as f:
        json.dump(myjson, f)